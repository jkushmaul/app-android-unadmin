# android-unadmin

A skeleton gradle project which will make hacking APKs easier.  It's just packaging and groovy to facilitate a few things.

The pilot project for this was MsOutlook.  You can literally use the steps below to get the original apk, set some properties using this  
as an existing project, and the result is a completely oblivious app that thinks it has device administration - but in no wy does.

I just wanted my calendar on my phone.  But work requires device admin and it is annoying to me that this is just "OK" with people.
Complete access to your phone.  (Go read the source for DeviceAdminManager and what they can do).  I'd be surprised they couldn't unlock my 
Samsung snapdragon bootloader for me even though it's not possible on any xna forum.

Perhaps a bit thumb-nosed as well... :)
 

# Disclaimer
I do not provide a mechanism to get original APKs but generic instructions are below.
There are likely legal issues or contractual issues being violated if proceed with an unauthorized app.
 

This isn't the greatest project in the world, it's just a tribute.    

# What
1. Decompile to original smali
1. Build support APK
1. Extract signature from original using support APK androidTest
1. Decompile mock objects from support APK to mocked smali 
1. Merge mocked objects into original smali
1. Implant mocks by using simple regex replacement rules
1. Patch AndroidManifest to replace device admin
1. Build + Sign APK
1. Push to device
1. Start app in debug mode, waiting for debugger
1. Run -> Attach Debugger to Android Process (select the running app)
1. Logcat -> select the running app.


# Future
I want to stop requiring original to be installed for the signature extraction and instead,  
use an android tool to get the same result with the already existing original_apk property.
Otherwise it's possible to just get the signature of the mocked app today.

Try to shift more of this into a 3rd gradle project, some fairly common parts to this type of project
Manipulating the manifest, getting the signature, mocked objects and an easy way to apply custom.
 
# Legacy follows
Below is effectively a journal of what I went through to get to the above.

## Install Anbox

    sudo snap install anbox

## Install apktool

 1. https://github.com/iBotPeaches/Apktool

## Install opengapps
 1. https://raw.githubusercontent.com/geeks-r-us/anbox-playstore-installer/master/install-playstore.sh
 2. ./install-opengapps.sh
 3. Start anbox.appmgr
	 3a. Give play services and play store all permissions
	 3b. Close
 4. Start anbox.appmgr
	 4a. Sign into google

## Acquire microsoft outlook apk

 1. Install ${package_name} from opengapps play store
 2. Close anbox.appmgr
 3. `cp /var/snap/anbox/common/data/app/${package_name}-1/base.apk ./${package_name}.orig.apk`
 4. `chmod 400 ${package_name}.orig.apk`
 5. 

## Decompile APK

 1. `java -jar ~/tools/apktool/apktool_2.4.0.jar d com.microsoft.outlook.orig.apk -o decompiled/`
 2. `cd decompiled`
 3. `git init && git add . && git commit -a -m "Initial commit"`

## IDE
 1. Android Studio
 2. Do what you do to smali

## Recompile APK

 1. `java -jar ~/tools/apktool/apktool_2.4.0.jar b decompiled/ -o com.microsoft.fucked.apk`
 2. `keytool -genkey -v -keystore 0utl00k.keystore -alias 0utl00k -keyalg RSA -keysize 2048 -validity 10000`
 3. `jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore 0utl00k.keystore com.microsoft.fucked.apk 0utl00k`

## Uninstall orig ${package_name}

 1. Because the signing key differs, android will not update the app.
 2. Uninstall it first then install new apk.

## Install new outlook

 1. Use adb push or in anbox, just copy to /var/snap/anbox/common/data/media/0/Download
 2. Browse to file in android and click to install
 3. Allow install from unknown sources, in fact, just turn off google->security->play protect/*  
    I'm not quite sure if turning off play protect is completely necessary anymore
 4. Install again, and make sure to not click "OK" but "INSTALL ANYWAY" (self signed)

## Enjoy
Enjoy being able to check your email without your overbearing IT department, or (increasingly) hostile government having full access to your device.

