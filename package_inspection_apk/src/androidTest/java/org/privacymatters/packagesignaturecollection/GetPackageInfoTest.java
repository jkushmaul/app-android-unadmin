package org.privacymatters.packagesignaturecollection;

import android.Manifest.permission;
import android.app.Instrumentation;
import android.app.Instrumentation.ActivityResult;
import android.content.pm.InstrumentationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.os.Environment;
import android.test.InstrumentationTestRunner;
import android.test.InstrumentationTestSuite;
import android.util.Log;

import androidx.test.InstrumentationRegistry;
import androidx.test.internal.runner.InstrumentationConnection;
import androidx.test.rule.ActivityTestRule;
import androidx.test.rule.GrantPermissionRule;
import androidx.test.runner.AndroidJUnit4;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import junit.framework.Assert;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;


/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class GetPackageInfoTest  {

    @Rule
    public GrantPermissionRule mRuntimePermissionRule = GrantPermissionRule .grant(permission.WRITE_EXTERNAL_STORAGE);

    public void buildPackageInfoFile(PackageInfo packageInfo, File outputFile) throws IOException {
        final Signature[] signatures = packageInfo.signatures;
        StringBuilder sb = new StringBuilder();
        sb.append(String.format("versioncode=%d\n", packageInfo.versionCode));
        sb.append(String.format("versionname=%s\n", packageInfo.versionName));
        sb.append(String.format("total_sigs=%d\n", signatures.length));
        for (int i = 0; i < signatures.length; i++) {
            Signature signature = signatures[i];
            String sig = signature.toCharsString();
            sb.append(String.format("sig[%d]=%s\n", i, sig));
        }
        String packageInfoStr = sb.toString();
        Assert.assertFalse(packageInfoStr.isEmpty());
        outputFile.createNewFile();
        FileWriter writer = new FileWriter(outputFile);
        writer.append(packageInfoStr);
        writer.close();
    }

    @Test
    public void getSignaturesTestRunner() {
        System.out.println("getSignatures()");

        final String packageName = BuildConfig.TARGET_PACKAGE_NAME;
        final PackageInfo packageInfo;


        File f = new File("/sdcard/",  packageName + ".packageinfo");

        try {
            packageInfo = InstrumentationRegistry.getInstrumentation().getContext()
                .getPackageManager().getPackageInfo(packageName, PackageManager.GET_SIGNATURES);

            buildPackageInfoFile(packageInfo, f);

        } catch (NameNotFoundException | IOException e) {
            Log.e("packageinfo", "failed", e);
            System.err.println("getSignatures failed: " + e.getMessage());
            e.printStackTrace(System.err);
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);

            Assert.fail("getSignatures() failed: " + e.getMessage() + "\n" + sw.toString() + "\nf.path=" + f.getAbsolutePath());
        }
    }

}
