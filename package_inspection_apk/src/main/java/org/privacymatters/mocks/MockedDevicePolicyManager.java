package org.privacymatters.mocks;

import android.content.ComponentName;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MockedDevicePolicyManager {
  public static final int ENCRYPTED = 0x03;
  public static final int PASSWORD_LENGTH = 0xff;

  private Set<ComponentName> componentNames = new HashSet<>();

  public List<ComponentName> getActiveAdmins() {
    List<ComponentName> newList = new ArrayList<>();
    newList.addAll(componentNames);
    return newList;
  }


  public boolean isActivePasswordSufficient() {
    return true;
  }
  public boolean isAdminActive(ComponentName admin) {
    componentNames.add(admin);
    return true;
  }

  public int setStorageEncryption(ComponentName admin, boolean encrypt) {
    return ENCRYPTED;
  }
  public int getStorageEncryptionStatus() {
    return ENCRYPTED;
  }

  public int getPasswordMinimumLength() {
    return PASSWORD_LENGTH;
  }
  public int getPasswordMinimumLength(ComponentName admin) {
    return getPasswordMinimumLength();
  }
  public void setPasswordMinimumLength(ComponentName admin, int length) {

  }
  public void setMaximumTimeToLock(ComponentName admin, long milis) {

  }
  public void setPasswordQuality(ComponentName admin, int quality) {

  }



}
