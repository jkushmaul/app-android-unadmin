package org.privacymatters.mocks;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.Signature;
import android.util.Log;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;
import junit.framework.Assert;

/** Replace:
 *  invoke-virtual {v1, p0}, Landroid/content/pm/PackageManager;->getInstallerPackageName(Ljava/lang/String;)Ljava/lang/String;
 *  with
 *  invoke-static {v1, p0}, Lorg/privacymatters/mocks/PackageManager;->getInstallerPackageName(Ljava/lang/String;)Ljava/lang/String;
 */
public class MockedPackageManager {
  private static final String GOOGLE_PLAYSTORE_INSTALLER = "com.android.vending";


  public static final int versionCode;
  public static final String versionName;
  public static final Signature[] signatures;
  public static Signature[] getSignatures() {
    return signatures;
  }

  static {
    versionCode = Integer.parseInt(MockProperties.VERSION_CODE);
    versionName = MockProperties.VERSION_NAME;
    int totalSigs = 1;
    signatures = new Signature[totalSigs];
    signatures[0] = new Signature(MockProperties.SIGNATURE);
  }


  public static String getInstallerPackageName(String name) {
    return GOOGLE_PLAYSTORE_INSTALLER;
  }

  private static void smaliCheatsheet() {
    MockedPackageManager.getInstallerPackageName("horf");
    Signature[] s = MockedPackageManager.signatures;
    s = MockedPackageManager.getSignatures();
  }
}
