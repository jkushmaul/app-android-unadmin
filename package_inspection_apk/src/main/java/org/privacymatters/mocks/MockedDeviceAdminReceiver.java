package org.privacymatters.mocks;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public abstract class MockedDeviceAdminReceiver extends BroadcastReceiver {

  /*
  The Receiver is expected to be able to receive these intents:
  <intent-filter>
      <action android:name="android.app.action.DEVICE_ADMIN_ENABLED"/>
      <action android:name="android.app.action.DEVICE_ADMIN_DISABLE_REQUESTED"/>
      <action android:name="android.app.action.DEVICE_ADMIN_DISABLED"/>
      <action android:name="com.microsoft.office.outlook.ACTION.UPDATE_POLICIES"/>
    </intent-filter>

   */

  /* Normally, DEVICE_ADMIN will launch the android device admin activity.
  Once user actually activates device admin, DeviceAdminActivity then fires a broadcast to "DEVICE_ADMIN_ENABLED"
  which launches DeviceAdminReceiver base, which then passes that through to "onEnabled(context, DEVICE_ADMIN_ENABLED")

  //This just skips the activity step and translates DEVICE_ADMIN -> DEVICE_ADMIN_ENABLED
  */

  //This is activated from DEVICE_ADMIN intent
  public static final String DEVICE_ADMIN = "org.privacymatters.mocks.DEVICE_ADMIN";

  //This then calls onEnabled with the DEVICE_ADMIN_ENABLED Intent
  public static final String DEVICE_ADMIN_ENABLED = "org.privacymatters.mocks.intents.DEVICE_ADMIN_ENABLED";
  //I don't know the difference
  public static final String ACTION_DEVICE_ADMIN_ENABLED = "org.privacymatters.mocks.intents.ACTION_DEVICE_ADMIN_ENABLED";

  public abstract void onEnabled(Context context, Intent intent);

  @Override
  public void onReceive(Context context, Intent intent) {
    if (intent.getAction() == DEVICE_ADMIN) {
      Intent newIntent = new Intent(DEVICE_ADMIN_ENABLED);
      onEnabled(context, newIntent);
    }
  }
}
