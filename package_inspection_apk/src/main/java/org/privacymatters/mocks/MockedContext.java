package org.privacymatters.mocks;

import android.content.Context;

public class MockedContext {
  public static final String DEVICE_POLICY = "device_policy";

  public static Object getSystemService(Context c, String s) {
    if (DEVICE_POLICY.equals(s)) {
      return new MockedDevicePolicyManager();
    } else {
      return c.getSystemService(s);
    }
  }


  private static void smaliCheatsheat() {
    Context c = null;
    String s = null;
    Object o = MockedContext.getSystemService(c, s);
  }
}
