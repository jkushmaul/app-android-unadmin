package org.privacymatters.mocks;

import android.content.Context;
import java.io.IOException;
import java.io.StringReader;
import java.util.Properties;

public class MockProperties {
    public static final String VERSION_CODE = "${versionCode}";
    public static final String VERSION_NAME = "${versionName}";
    public static final String SIGNATURE = "${signature}";
}
