package org.privacymatters.mocks;

import android.app.Activity;

public class MockedActivity {
    public static Object getSystemService(Activity a, String s) {
      if (MockedContext.DEVICE_POLICY.equals(s)) {
        return new MockedDevicePolicyManager();
      } else {
        return a.getSystemService(s);
      }
    }
}
