package org.privacymatters;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Stub {

  private static Stub[] stubsToImplant = new Stub[]{new Stub(
      "Landroid/content/pm/PackageManager;->getInstallerPackageName",
      regscape("invoke-virtual {") + "[a-z][0-9], ([a-z][0-9])" + regscape(
          "}, Landroid/content/pm/PackageManager;->getInstallerPackageName(Ljava/lang/String;)Ljava/lang/String;"),
      "invoke-static {$1}, Lorg/privacymatters/mocks/MockedPackageManager;->getInstallerPackageName(Ljava/lang/String;)Ljava/lang/String;")

      , new Stub(
      "Landroid/content/pm/PackageInfo;->signatures",
      regscape("iget-object ") + "([a-z][0-9]), [a-z][0-9]" + regscape(
          ", Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;"),
      "sget-object $1, Lorg/privacymatters/mocks/MockedPackageManager;->signatures:[Landroid/content/pm/Signature;")

      , new Stub(
      "Landroid/content/pm/PackageInfo;->getSignatures()[Landroid/content/pm/Signature;",
      regscape("iget-object ") + "([a-z][0-9]), [a-z][0-9]" + regscape(
          ", Landroid/content/pm/PackageInfo;->getSignatures()[Landroid/content/pm/Signature;"),
      "sget-object $1, Lorg/privacymatters/mocks/MockedPackageManager;->getSignatures()[Landroid/content/pm/Signature;")

      , new Stub(
      "Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;",
      regscape("invoke-virtual {") + "([0-9a-z, ]+)" + regscape(
          "}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;"),
      "invoke-static {$1}, Lorg/privacymatters/mocks/MockedContext;->getSystemService(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/Object;")


      , new Stub(
      "Landroid/app/admin/DevicePolicyManager;",
      "Landroid/app/admin/DevicePolicyManager;",
      "Lorg/privacymatters/mocks/MockedDevicePolicyManager;")
      , new Stub(
      "android.app.extra.DEVICE_ADMIN",
      regscape("android.app.extra.DEVICE_ADMIN"),
      "org.privacymatters.mocks.DEVICE_ADMIN")
      , new Stub(
      "Landroid/app/admin/DeviceAdminReceiver;",
      "Landroid/app/admin/DeviceAdminReceiver;",
      "Lorg/privacymatters/mocks/MockedDeviceAdminReceiver;")
    };

 /*
    This is a problem:
    invoke-virtual {v6, v0}, Lcom/acompli/acompli/CentralActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    I'd have to actually know that CentralActivity is an activity (so some form of smali knowledge)
    I don't want to make a specific stub for this because it will be common.
    so {
      if most super class has .super Landroid/app/Activity; then
         match
            and replace
            invoke-virtual {v6, v0}, Lcom/acompli/acompli/CentralActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
            with
            invoke-static {v6, v0}, Lorg/privacymatters/mocks/MockedActivity;->getSystemService(LLjava/lang/String;)Ljava/lang/Object;
    }
  */
  public static final Stub ACTIVITY_GETSYSTEMSERVICE = new Stub(
      "getSystemService",
          regscape("invoke-virtual {") + "([0-9a-z, ]+)" + regscape("},") +".*" + regscape("->getSystemService(Ljava/lang/String;)Ljava/lang/Object;"),
          "invoke-static {$1}, Lorg/privacymatters/mocks/MockedActivity;->getSystemService(Landroid/app/Activity;Ljava/lang/String;)Ljava/lang/Object;");

  public static String regscape(String regex) {
    return regex.replaceAll("[\\W]", "\\\\$0");
  }

  private String literal;

  public String getLiteral() {
    return literal;
  }

  private Pattern pattern;
  private String replacement;

  private Stub(String literal, String pattern, String replacement) {
    try {
      this.pattern = Pattern.compile(pattern);
    } catch (Exception ex) {
      System.err.println(String.format("Error compiling regex: %s\n%s", pattern, ex.getMessage()));
      ex.printStackTrace(System.err);
    }
    this.literal = literal;
    this.replacement = replacement;
  }

  public String replace(String line) {
    return replace(false, line);
  }

  public String replace(boolean is_debug, String line) {
    if (line.contains(literal)) {
      if (is_debug) System.out.println(String.format("Stub, Contains literal: %s", literal));
      Matcher matcher = pattern.matcher(line);
      if (matcher.find()) {
        return matcher.replaceFirst(replacement);
      }
    }
    return null;
  }



  public static String implantStubs(boolean isActivity, String line) {
    if (isActivity) {
      String result = ACTIVITY_GETSYSTEMSERVICE.replace(line);
      if (result != null) {
        return result;
      }
    }
    for (int i = 0; i < stubsToImplant.length; i++) {
      String result = stubsToImplant[i].replace(line);
      if (result != null) {
        return result;
      }
    }
    return null;
  }
}
