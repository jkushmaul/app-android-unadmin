#!/bin/bash
function die () {
   echo $1
   exit -1
}

ls apk/src || die "apk/src/ not found"
ls patches/ || die "patches/ not found"
ls scripts/ || die "scripts/ not found"

git apply --verbose --no-index --directory apk/src/ patches/*
