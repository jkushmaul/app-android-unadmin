#!/bin/bash
set -e
function die() {
   echo $1
   exit -1
}

test "$ANDROID_SERIAL" != "" || die "You must define an environment variable, ANDROID_SERIAL the serial of the device to be pushed to"
adb devices  2>/dev/null | grep -q "$ANDROID_SERIAL" || die "No device was found with ANDROID_SERIAL=$ANDROID_SERIAL"

APK="build/apk.apk"
ls "$APK" || die "Path not found: $APK"
ls src/ || die "src/ not found"
ls patches/ || die "patches/ not found"
ls scripts/ || die "scripts/ not found"

adb uninstall com.microsoft.office.outlook 2>/dev/null  || echo "Not installed"
adb install "$APK"

