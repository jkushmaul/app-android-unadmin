#!/bin/bash
function die() {
   echo $1
   exit -1
}

APK="$1"
test "$1" != "" || die "usage: decompileit.sh <apk>"
ls "$APK" || die "Path not found: $APK"
ls src/ || die "src/ not found"
ls patches/ || die "patches/ not found"
ls scripts/ || die "scripts/ not found"

java -jar ~/tools/apktool/apktool_2.4.0.jar d $APK  -o src/
