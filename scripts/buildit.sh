#!/bin/bash
set -e

function die() {
   echo $1
   exit -1
}
echo "$KEYSTORE"
echo "$KEYSTORE_PASS"

test "$KEYSTORE" != "" || die "You must define an environment variable, KEYSTORE, the path of the java keystore"
test "$KEYSTORE_PASS" != "" || die "You must define an environment variable, KEYSTORE_PASS, the keystore password"

ls "$KEYSTORE" || die "The keystore could not be found at path: $KEYSTORE"
keytool -list -keystore "$KEYSTORE"  -storepass "$KEYSTORE_PASS" > /dev/null || die "$KEYSTORE could not be verified"

APK="build/apk.apk"
ls build/ || die "build/ not found"
ls apk/src/ || die "src/ not found"
ls patches/ || die "patches/ not found"
ls scripts/ || die "scripts/ not found"


ls /dev/ashmem || sudo modprobe ashmem_linux 
ls /dev/binder || sudo modprobe binder_linux


java -jar ~/tools/apktool/apktool_2.4.0.jar b -d ./apk/src -o "$APK" && \
  jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore "$KEYSTORE" -keypass testtest -storepass testtest "$APK" 0utl00k


echo "Success"
