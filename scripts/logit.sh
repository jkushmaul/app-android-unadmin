#!/bin/bash
set -e

echo "Starting app, waiting for debug..."
adb shell am start -n com.microsoft.office.outlook/.MainActivity
sleep 1
adb logcat --pid=`adb shell pidof -s com.microsoft.office.outlook`
#adb logcat | grep -v -E 'Telephony|zygote64|Tethering|Mms|Samsung|LocSvc|QC-NETMGR|CarrierText|QMI_FW|EPDG|propClient|PerMgrSrv|BtGatt|ScanRecord|ConnectivityService|MotionRecognitionService|DataRouter|Bluetooth|NetworkController|chatty|bt_btm|AudioService|AudioPolicyManager|scanbledata|autoconnect|qti_sensors|SensorManager|BleProfile|scanopengatt|io_stats|Notification|bt_btif|BleManager|BtConfig|BatteryService|WIFI|SemContext|Finsky|DisplayPowerController|PowerManagerService|ProximityManager|oneconnect|SearchServiceStarter|RecordDBManager|Headset|WifiPermissionsUtil|Connection4O|BistoLogger|WifiScanningService'
