#!/bin/bash
set -e
set -x

trap "adb shell am force-stop com.microsoft.office.outlook" SIGINT

echo "Clearing out existing listeners"
adb shell am force-stop com.microsoft.office.outlook

echo "Starting app, waiting for debug..."
adb shell am start -D -n com.microsoft.office.outlook/.MainActivity
sleep 1
JDWP=$(timeout 1 adb jdwp ) || true
test "$JDWP" != "" || (echo "Failed to get JDWP, aborting"; adb shell am force-stop com.microsoft.office.outlook; exit -1)
adb forward tcp:5005 jdwp:$JDWP
echo "You may now debug.  Ctl-C to stop logcat will terminate app."
read -t 5 -p "Starting logcat in 5 seconds, or any key to start immediately..." || true

adb logcat --pid=`adb shell pidof -s com.microsoft.office.outlook`
#adb logcat | grep -v -E 'Telephony|zygote64|Tethering|Mms|Samsung|LocSvc|QC-NETMGR|CarrierText|QMI_FW|EPDG|propClient|PerMgrSrv|BtGatt|ScanRecord|ConnectivityService|MotionRecognitionService|DataRouter|Bluetooth|NetworkController|chatty|bt_btm|AudioService|AudioPolicyManager|scanbledata|autoconnect|qti_sensors|SensorManager|BleProfile|scanopengatt|io_stats|Notification|bt_btif|BleManager|BtConfig|BatteryService|WIFI|SemContext|Finsky|DisplayPowerController|PowerManagerService|ProximityManager|oneconnect|SearchServiceStarter|RecordDBManager|Headset|WifiPermissionsUtil|Connection4O|BistoLogger|WifiScanningService'
